## -------------------------------------------------------------------------
## @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
## -------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.10)

project(ParametricFunctions)

# -- Output dirs
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})

# -- Packages
find_package(Boost REQUIRED)
find_package(OpenGL COMPONENTS OpenGL REQUIRED)
find_package(GLUT REQUIRED)

# -- Code directories
subdirs(apps lib)

## eof - CMakeLists.txt
