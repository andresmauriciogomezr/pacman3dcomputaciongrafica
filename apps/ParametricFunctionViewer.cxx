// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================

#include <string>

#include <PUJ_CGraf/AmbientLight.h>
#include <PUJ_CGraf/Controller.h>
#include <PUJ_CGraf/Cylinder.h>
#include <PUJ_CGraf/Sphere.h>
#include <PUJ_CGraf/SphericalCamera.h>
#include <PUJ_CGraf/Tablero.h>
#include <PUJ_CGraf/SpotLight.h>
#include <PUJ_CGraf/World.h>

int main( int argc, char** argv )
{
  PUJ_CGraf::World world;

  PUJ_CGraf::Cylinder tronco;
  tronco.nombre = "t";
  tronco.set_radius(.5);
  tronco.set_height(2);
  tronco.sample( 10, 10 );
  // tronco.rotacion[0] = 0;
  tronco.rotacion[0] = -90;
  tronco.rotacion[1] = 1;
  tronco.rotacion[2] = 0;
  tronco.rotacion[3] = 0;
  tronco.traslacion[0] = 3;
  tronco.traslacion[1] = 0;
  tronco.traslacion[2] = -0.8;

  PUJ_CGraf::Cylinder cuello;
  cuello.nombre = "C";
  cuello.set_radius(.1);
  cuello.set_height(.2);
  cuello.sample( 10, 10 );
  cuello.traslacion[0] = 0;
  cuello.traslacion[1] = 0;
  cuello.traslacion[2] = -.2;
  tronco.add_child(&cuello);


  PUJ_CGraf::Sphere cabeza;
  cabeza.nombre = "c";
  cabeza.set_radius(.3);
  cabeza.sample( 10, 10 );
  cabeza.traslacion[0] = 0;
  cabeza.traslacion[1] = 0;
  cabeza.traslacion[2] = -.2;
  cabeza.color[0] = 255;
  cuello.add_child(&cabeza);




  PUJ_CGraf::Cylinder brazo_izquierdo;
  brazo_izquierdo.nombre = "i";
  brazo_izquierdo.set_radius(.2);
  brazo_izquierdo.set_height(.5);
  brazo_izquierdo.sample( 10, 10 );
  brazo_izquierdo.traslacion[0] = 0;
  brazo_izquierdo.traslacion[1] = .5;
  brazo_izquierdo.traslacion[2] = 0;
  brazo_izquierdo.rotacion[0] = -90;
  brazo_izquierdo.rotacion[1] = 1;
  brazo_izquierdo.rotacion[2] = 0;
  brazo_izquierdo.rotacion[3] = 0;

  PUJ_CGraf::Cylinder ante_brazo_izquierdo;
  ante_brazo_izquierdo.nombre = "o";
  ante_brazo_izquierdo.set_radius(.1);
  ante_brazo_izquierdo.set_height(.5);
  ante_brazo_izquierdo.sample( 10, 10 );
  ante_brazo_izquierdo.traslacion[0] = 0;
  ante_brazo_izquierdo.traslacion[1] = 0;
  ante_brazo_izquierdo.traslacion[2] = .5;
  brazo_izquierdo.add_child(&ante_brazo_izquierdo);

  tronco.add_child(&brazo_izquierdo);
  

  PUJ_CGraf::Cylinder brazo_derecho;
  brazo_derecho.nombre = "d";
  brazo_derecho.set_radius(.2);
  brazo_derecho.set_height(.5);
  brazo_derecho.sample( 10, 10 );
  brazo_derecho.traslacion[0] = 0;
  brazo_derecho.traslacion[1] = -.5;
  brazo_derecho.traslacion[2] = 0;
  brazo_derecho.rotacion[0] = 90;
  brazo_derecho.rotacion[1] = 1;
  brazo_derecho.rotacion[2] = 0;
  brazo_derecho.rotacion[3] = 0;

  PUJ_CGraf::Cylinder ante_brazo_derecho;
  ante_brazo_derecho.nombre = "f";
  ante_brazo_derecho.set_radius(.1);
  ante_brazo_derecho.set_height(.5);
  ante_brazo_derecho.sample( 10, 10 );
  ante_brazo_derecho.traslacion[0] = 0;
  ante_brazo_derecho.traslacion[1] = 0;
  ante_brazo_derecho.traslacion[2] = .5;
  ante_brazo_derecho.rotacion[0] = 90;
  ante_brazo_derecho.rotacion[1] = 0;
  ante_brazo_derecho.rotacion[2] = 1;
  ante_brazo_derecho.rotacion[3] = 0;
  brazo_derecho.add_child(&ante_brazo_derecho);

  tronco.add_child(&brazo_derecho);




  PUJ_CGraf::Cylinder pierna_izquierda;
  pierna_izquierda.nombre = "I";
  pierna_izquierda.set_radius(.2);
  pierna_izquierda.set_height(.5);
  pierna_izquierda.sample( 10, 10 );
  pierna_izquierda.traslacion[0] = 0;
  pierna_izquierda.traslacion[1] = .25;
  pierna_izquierda.traslacion[2] = 2;


  PUJ_CGraf::Cylinder canilla_izquierda;
  canilla_izquierda.nombre = "O";
  canilla_izquierda.set_radius(.1);
  canilla_izquierda.set_height(.5);
  canilla_izquierda.sample( 10, 10 );
  canilla_izquierda.traslacion[0] = 0;
  canilla_izquierda.traslacion[1] = 0;
  canilla_izquierda.traslacion[2] = .5;
  pierna_izquierda.add_child(&canilla_izquierda);


  tronco.add_child(&pierna_izquierda);
  

  PUJ_CGraf::Cylinder pierna_derecha;
  pierna_derecha.nombre = "D";
  pierna_derecha.set_radius(.2);
  pierna_derecha.set_height(.5);
  pierna_derecha.sample( 10, 10 );
  pierna_derecha.traslacion[0] = 0;
  pierna_derecha.traslacion[1] = -.25;
  pierna_derecha.traslacion[2] = 2;

  PUJ_CGraf::Cylinder canilla_derecha;
  canilla_derecha.nombre = "F";
  canilla_derecha.set_radius(.1);
  canilla_derecha.set_height(.5);
  canilla_derecha.sample( 10, 10 );
  canilla_derecha.traslacion[0] = 0;
  canilla_derecha.traslacion[1] = 0;
  canilla_derecha.traslacion[2] = .5;
  pierna_derecha.add_child(&canilla_derecha);





  tronco.add_child(&pierna_derecha);
  // world.add_child( &tronco );



  PUJ_CGraf::Tablero obj;
  obj.nombre = "Tablero";
  obj.read_from_OBJ( "/home/parallels/Documents/iluminacion/data/source/marblemaze/marblemaze.obj" );
  //obj.read_from_OBJ( "/home/parallels/Documents/iluminacion/data/source/marblemaze/pacman.obj" );
  // obj.read_from_OBJ( "/home/parallels/Documents/iluminacion/data/source/marblemaze/phantom.obj" );
  //obj.sample( 10, 10 );
  //obj._update_bounding_box( );

  obj.rotacion[0] = -90;
  // obj.rotacion[0] = 0;
  obj.rotacion[1] = 1;
  obj.rotacion[2] = 0;
  obj.rotacion[3] = 0;

  tronco.parent = &obj;
  
  obj.add_child( &tronco );
  world.add_child( &obj );





  const float* bb = world.bounding_box( );

  PUJ_CGraf::SphericalCamera cam;
  cam.set_bounding_box( bb );

  PUJ_CGraf::AmbientLight l1;
  l1.set_color( 1, 1, 1 );

  PUJ_CGraf::SpotLight l[ 2 ];
  l[ 0 ].set_color( 0, 0, 1 );
  l[ 0 ].set_position( bb[ 0 ], bb[ 1 ], bb[ 2 ] );

  l[ 1 ].set_color( 0, 1, 0 );
  l[ 1 ].set_position( bb[ 3 ], bb[ 4 ], bb[ 5 ] );

  PUJ_CGraf::Controller::set_shade_type_to_flat( );
  PUJ_CGraf::Controller::add_light( &l1 );
  PUJ_CGraf::Controller::add_light( &l[ 0 ] );
  PUJ_CGraf::Controller::add_light( &l[ 1 ] );

  PUJ_CGraf::Controller::init(
    &argc, argv, &world, &cam, "Parametric function viewer", 700, 700
    );
  PUJ_CGraf::Controller::loop( );

  return( EXIT_SUCCESS );
}

// eof - ParametricFunctionViewer.cxx
