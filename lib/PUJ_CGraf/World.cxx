// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================

#include <PUJ_CGraf/World.h>

#include <limits>
#include <iostream>
using namespace std;

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

// -------------------------------------------------------------------------
PUJ_CGraf::World::
World( )
  : Superclass( )
{
}

// -------------------------------------------------------------------------
const PUJ_CGraf::World::
TReal* PUJ_CGraf::World::
bounding_box( )
{
  this->m_BBox[ 0 ]
    = this->m_BBox[ 1 ]
    = this->m_BBox[ 2 ]
    = std::numeric_limits< TReal >::max( );
  this->m_BBox[ 3 ]
    = this->m_BBox[ 4 ]
    = this->m_BBox[ 5 ]
    = std::numeric_limits< TReal >::lowest( );

  std::vector< Object* >::const_iterator d;
  for( d = this->m_Children.begin( ); d != this->m_Children.end( ); ++d )
  {
    const TReal* b = ( *d )->bounding_box( );
    for( unsigned int i = 0; i < 3; ++i )
      this->m_BBox[ i ] =
        ( b[ i ] < this->m_BBox[ i ] )? b[ i ]: this->m_BBox[ i ];
    for( unsigned int i = 3; i < 6; ++i )
      this->m_BBox[ i ] =
        ( this->m_BBox[ i ] < b[ i ] )? b[ i ]: this->m_BBox[ i ];
  } // end for

  return( this->m_BBox );
}

// -------------------------------------------------------------------------
void PUJ_CGraf::World::
set_texture(
  const unsigned char* buffer,
  const unsigned int& w, const unsigned int& h
  )
{
  // Do nothing
}

// -------------------------------------------------------------------------
void PUJ_CGraf::World::
draw( ) const
{
  std::vector< Object* >::const_iterator d;
  for( d = this->m_Children.begin( ); d != this->m_Children.end( ); ++d ){
    ( *d )->draw( );

    //cout <<  ( *d )->nombre << endl; 
    //cout << "" << endl;  
   
    glPushMatrix( );
    glTranslatef( .3, .3, 0 );
  }
}

// eof - World.cxx
