// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================
#ifndef __PUJ_CGraf__Plane__h__
#define __PUJ_CGraf__Plane__h__

#include <PUJ_CGraf/ParametricObject.h>

namespace PUJ_CGraf
{
  /**
   */
  class Tablero
    : public ParametricObject
  {
  public:
    using Self = Tablero;
    using Superclass = ParametricObject;
    using TReal = Superclass::TReal;
    using TNatural = Superclass::TNatural;
    using VReal = Superclass::VReal;
    using VNatural = Superclass::VNatural;

  public:
    Tablero( );
    virtual ~Tablero( ) = default;

    void set_center( const TReal& x, const TReal& y, const TReal& z );
    void set_width( const TReal& w );
    void set_height( const TReal& h );

    virtual void point( TReal* p, const TReal& u, const TReal& v ) const override;
    virtual void normal( TReal* n, const TReal& u, const TReal& v ) const override;
    virtual void draw( ) const;

    virtual bool vericiar_interseccion_caras( float x, float y, float z) const;


  protected:
    TReal m_Center[ 3 ] { 0 };
    TReal m_Width  { 1 };
    TReal m_Height { 1 };
  };
} // end namespace

#endif // __PUJ_CGraf__Plane__h__

// eof - Plane.h
