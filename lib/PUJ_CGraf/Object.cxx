// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================

#include <PUJ_CGraf/Object.h>

#include <cstring>
#include <limits>
#include <GL/gl.h>


#include <fstream>
#include <sstream>

#include <boost/tokenizer.hpp>

// -------------------------------------------------------------------------
PUJ_CGraf::Object::
Object( )
{
}

// -------------------------------------------------------------------------
const PUJ_CGraf::Object::
TReal* PUJ_CGraf::Object::
bounding_box( )
{
  return( this->m_BBox );
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Object::
set_texture(
  const unsigned char* buffer,
  const unsigned int& w, const unsigned int& h
  )
{
  this->m_TexBuffer.clear( );
  this->m_TexBuffer.resize( w * h * 3 );
  this->m_TexBuffer.shrink_to_fit( );
  std::memcpy(
    this->m_TexBuffer.data( ), buffer, w * h * 3 * sizeof( unsigned char )
    );
  this->m_TexWidth = w;
  this->m_TexHeight = h;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Object::
add_child( Object* c )
{
  this->m_Children.push_back( c );
}

// -------------------------------------------------------------------------
PUJ_CGraf::Object* PUJ_CGraf::Object::
child( const unsigned int& i )
{
  if( i < this->m_Children.size( ) )
    return( this->m_Children[ i ] );
  else
    return( nullptr );
}

// -------------------------------------------------------------------------
const PUJ_CGraf::Object* PUJ_CGraf::Object::
child( const unsigned int& i ) const
{
  if( i < this->m_Children.size( ) )
    return( this->m_Children[ i ] );
  else
    return( nullptr );
}

// -------------------------------------------------------------------------
unsigned int PUJ_CGraf::Object::
number_of_children( ) const
{
  return( this->m_Children.size( ) );
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Object::
draw( ) const
{
  std::map< TNatural, VNatural >::const_iterator c;
  VNatural::const_iterator v;
  const TReal* p = this->m_Points.data( );
  const TReal* pn = this->m_PointNormals.data( );


  glRotatef(this->rotacion[0], this->rotacion[1], this->rotacion[2], this->rotacion[3]);
  
  glPushMatrix( );
  // glPopMatrix();

  for( c = this->m_Cells.begin( ); c != this->m_Cells.end( ); ++c )
  {
    if( c->first == 1 )
      glBegin( GL_POINTS );
    else if( c->first == 2 )
      glBegin( GL_LINES );
    else if( c->first == 3 )
      glBegin( GL_TRIANGLES );
    else if( c->first == 4 )
      glBegin( GL_QUADS );
    for( v = c->second.begin( ); v != c->second.end( ); ++v )
    {
      if( pn != nullptr )
        glNormal3fv( pn + ( *v * 3 ) );
      glVertex3fv( p + ( *v * 3 ) );
    } // end for
    glEnd( );
  } // end for

  std::vector< Object* >::const_iterator d;
  for( d = this->m_Children.begin( ); d != this->m_Children.end( ); ++d ){
   

    glPushMatrix( );
    glTranslatef( ( *d )->traslacion[0], ( *d )->traslacion[1], ( *d )->traslacion[2] );
    
    ( *d )->draw( );

  }
  // glPopMatrix();
  // glPopMatrix();

  glPopMatrix();
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Object::
_update_bounding_box( )
{
  // Update bounding box
  this->m_BBox[ 0 ]
    = this->m_BBox[ 1 ]
    = this->m_BBox[ 2 ]
    = std::numeric_limits< TReal >::max( );
  this->m_BBox[ 3 ]
    = this->m_BBox[ 4 ]
    = this->m_BBox[ 5 ]
    = std::numeric_limits< TReal >::lowest( );
  for( unsigned int i = 0; i < this->m_Points.size( ); i += 3 )
  {
    for( unsigned int j = 0; j < 3; ++j )
    {
      if( this->m_Points[ i + j ] < this->m_BBox[ j ] )
        this->m_BBox[ j ] = this->m_Points[ i + j ];
      if( this->m_BBox[ j + 3 ] < this->m_Points[ i + j ] )
        this->m_BBox[ j + 3 ] = this->m_Points[ i + j ];
    } // end for
  } // end for

}


bool PUJ_CGraf::Object::
   verificarNombre(string nombre){

      for( auto c: this->m_Children ){
        c->rotando = false;
        if(c->verificarNombre(nombre)){
          //return true;
        }
      }
    
      if(this->nombre == nombre){
        this->rotando = true;
        std::cout << this->nombre << endl;
        return true;
      }
      
      this->rotando = false;

      
      return false;
  }



void PUJ_CGraf::Object::
read_from_OBJ( const std::string& in_fname )
{
          std::cout<<"pasa"<<endl;

  using _S = boost::char_separator< char >;
  using _T = boost::tokenizer< _S >;
  using _L = std::vector< std::string >;

  // Read file buffer into memory
  std::ifstream in_file( in_fname.c_str( ) );
  std::istringstream in_str(
    std::string( std::istreambuf_iterator< char >{ in_file }, { } )
    );
  in_file.close( );

  // Prepare internal data
  this->m_Points.clear( );
  this->m_Normals.clear( );
  this->m_Faces.clear( );
  this->m_PointNormals.clear( );

  // Read line by line
  _S sep( ", \t\n" ), sep2( ", \t\n/" );
  std::string line;
  while( std::getline( in_str, line ) )
  {
          // std::cout<<"aaaa"<<endl;

    _T tok( line, sep );
    _L t;
    t.insert( t.begin( ), tok.begin( ), tok.end( ) );
    if( t.size( ) > 0 )
    {
      if( t[ 0 ] == "v" )
      {
        if( t.size( ) >= 4 )
        {
          this->m_Points.push_back( std::atof( t[ 1 ].c_str( ) ) );
          this->m_Points.push_back( std::atof( t[ 2 ].c_str( ) ) );
          this->m_Points.push_back( std::atof( t[ 3 ].c_str( ) ) );
        } // end if
      }
      else if( t[ 0 ] == "vn" )
      {
        if( t.size( ) >= 4 )
        {
          this->m_Normals.push_back( std::atof( t[ 1 ].c_str( ) ) );
          this->m_Normals.push_back( std::atof( t[ 2 ].c_str( ) ) );
          this->m_Normals.push_back( std::atof( t[ 3 ].c_str( ) ) );
        } // end if
      }
      else if( t[ 0 ] == "f" )
      {
        std::vector< unsigned int > f, pn;
        for( unsigned int i = 1; i < t.size( ); ++i )
        {
          _T tok2( t[ i ], sep2 );
          _L t2;
          t2.insert( t2.begin( ), tok2.begin( ), tok2.end( ) );
          f.push_back( std::atoi( t2[ 0 ].c_str( ) ) - 1 );
          if( t2.size( ) == 2 )
            pn.push_back( std::atoi( t2[ 1 ].c_str( ) ) - 1 );
        } // end for
        this->m_Faces.push_back( f );
        // this->m_Cells.push_back( f );

        if( pn.size( ) > 0 )
        {
          std::cout<<"sssssss"<<endl;
          // this->m_PointNormals.push_back( pn );

        }
      } // end if
    } // end if
  } // end while

  // Free unused memory
  this->m_Points.shrink_to_fit( );
  this->m_Normals.shrink_to_fit( );
  this->m_Faces.shrink_to_fit( );
  this->m_PointNormals.shrink_to_fit( );

  // Update bounding box
  this->m_BBox[ 0 ]
    = this->m_BBox[ 1 ]
    = this->m_BBox[ 2 ]
    = std::numeric_limits< float >::max( );
  this->m_BBox[ 3 ]
    = this->m_BBox[ 4 ]
    = this->m_BBox[ 5 ]
    = std::numeric_limits< float >::lowest( );
  for( unsigned int i = 0; i < this->m_Points.size( ); i += 3 )
  {
    for( unsigned int j = 0; j < 3; ++j )
    {
      if( this->m_Points[ i + j ] < this->m_BBox[ j ] )
        this->m_BBox[ j ] = this->m_Points[ i + j ];
      if( this->m_BBox[ j + 3 ] < this->m_Points[ i + j ] )
        this->m_BBox[ j + 3 ] = this->m_Points[ i + j ];
    } // end for
  } // end for
}





bool PUJ_CGraf::Object::
vericiar_interseccion_caras( float x, float y, float z)
const {
 
}



// eof - Object.cxx
