// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================
#ifndef __PUJ_CGraf__World__h__
#define __PUJ_CGraf__World__h__

#include <PUJ_CGraf/Object.h>
#include <iostream>

namespace PUJ_CGraf
{
  /**
   */
  class World final
    : public Object
  {
  public:
    using Self = World;
    using Superclass = Object;
    using TReal = Superclass::TReal;
    using TNatural = Superclass::TNatural;
    using VReal = Superclass::VReal;
    using VNatural = Superclass::VNatural;

  public:
    World( );
    virtual ~World( ) = default;

    virtual const TReal* bounding_box( ) override;
    virtual void set_texture(
      const unsigned char* buffer,
      const unsigned int& w, const unsigned int& h
      ) override;
    virtual void draw( ) const override;


    virtual void keypressed( unsigned char k ) override
    {
      // if( std::isdigit( k ) )
      //   this->m_Articulation = k - '0';
      // else if( k == 'x' )
      //   this->m_Joints[ this->m_Articulation ].rotate( 0.1, 1, 0, 0 );
      // else if( k == 'X' )
      //   this->m_Joints[ this->m_Articulation ].rotate( -0.1, 1, 0, 0 );
      // else if( k == 'y' )
      //   this->m_Joints[ this->m_Articulation ].rotate( 0.1, 0, 1, 0 );
      // else if( k == 'Y' )
      //   this->m_Joints[ this->m_Articulation ].rotate( -0.1, 0, 1, 0 );
      // else if( k == 'z' )
      //   this->m_Joints[ this->m_Articulation ].rotate( 0.1, 0, 0, 1 );
      // else if( k == 'Z' )
      //   this->m_Joints[ this->m_Articulation ].rotate( -0.1, 0, 0, 1 );
        for( auto c: this->m_Children ){
          std::string s;
          s += k; 
          
          if(k == 'c'|| k == 'C'|| k == 'i'|| k == 'I'  || k == 'd'|| k == 'D' || k == 'o'|| k == 'O' || k == 'f'|| k == 'F' || k == 't' ){
            c->verificarNombre(s);
          }

          c->keypressed( k );

        }

    }
  };
} // end namespace

#endif // __PUJ_CGraf__World__h__

// eof - World.h
