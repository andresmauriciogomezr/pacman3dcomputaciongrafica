// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================

#include <PUJ_CGraf/Tablero.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <cmath>
// -------------------------------------------------------------------------
PUJ_CGraf::Tablero::
Tablero( )
  : Superclass( )
{
  this->m_URange[ 0 ] = 0;
  this->m_URange[ 1 ] = 1;

  this->m_VRange[ 0 ] = 0;
  this->m_VRange[ 1 ] = 1;

  this->m_UClosed = false;
  this->m_VClosed = false;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Tablero::
set_center( const TReal& x, const TReal& y, const TReal& z )
{
  this->m_Center[ 0 ] = x;
  this->m_Center[ 1 ] = y;
  this->m_Center[ 2 ] = z;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Tablero::
set_width( const TReal& w )
{
  this->m_Width = w;
  this->m_URange[ 0 ] = 0;
  this->m_URange[ 1 ] = w;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Tablero::
set_height( const TReal& h )
{
  this->m_Height = h;
  this->m_VRange[ 0 ] = 0;
  this->m_VRange[ 1 ] = h;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Tablero::
point( TReal* p, const TReal& u, const TReal& v ) const
{
  // p[ 0 ] = u + this->m_Center[ 0 ];
  // p[ 1 ] = v + this->m_Center[ 1 ];
  // p[ 2 ] = 0;
}

// -------------------------------------------------------------------------
void PUJ_CGraf::Tablero::
normal( TReal* n, const TReal& u, const TReal& v ) const
{
  n[ 0 ] = 0;
  n[ 1 ] = 0;
  n[ 2 ] = 1;
}

void PUJ_CGraf::Tablero::
draw( ) const
{
  glRotatef(this->rotacion[0], this->rotacion[1], this->rotacion[2], this->rotacion[3]);
  // glPushMatrix( );

  // for( const auto& i: this->m_Points )
  // {
  //   std::cout<< this->m_Points[i] << endl;
  // }
  // std::cout<< "" << endl;
  
  const float* points = this->m_Points.data( );
  int contador = 0;
  for( const auto& face: this->m_Faces )
  {
    glBegin( GL_LINE_LOOP );
    {
      int contador2 = contador;
      for( const auto& i: face ){
        glVertex3fv( points + ( i * 3 ) );


        // std::cout<< "//////////" << endl;
        // std::cout<< face[i] << endl;
        // std::cout<< "//////////" << endl;

        // std::cout<< "" << endl;
        // std::cout<< i + 1 << endl;
        // int index = ( i * 3 );
        
        
        // std::cout<< this->m_Points[index] << endl;
        
        
        
        // std::cout<< std::to_string(this->m_Points[index]) + " " + std::to_string(this->m_Points[index + 1])+ " " + std::to_string(this->m_Points[index + 2])<< endl;
        // std::cout<< index << endl;
        // std::cout<< "////" << endl;

        // std::cout<< std::to_string(this->m_Points[contador]) + " " + std::to_string(this->m_Points[contador + 1])+ " " + std::to_string(this->m_Points[contador + 2])<< endl;

        contador2 ++;
      }
        // std::cout<< "" << endl;
        // std::cout<< "" << endl;

    }
    glEnd( );
    contador ++;

  } // end for
  // glPopMatrix();

  std::vector< Object* >::const_iterator d;
  for( d = this->m_Children.begin( ); d != this->m_Children.end( ); ++d ){
   
    // glPushMatrix( );
    glTranslatef( ( *d )->traslacion[0], ( *d )->traslacion[1], ( *d )->traslacion[2] );
    ( *d )->draw( );
  // glPopMatrix();
  }
  // glPopMatrix();

    // glPushMatrix( );
}

// eof - Plane.cxx



bool PUJ_CGraf::Tablero::
vericiar_interseccion_caras( float x, float y, float z)
const {
  x = std::round(x * 100) / 100;
  y = std::round(y * 100) / 100;
  z = std::round(z * 100) / 100;

  // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z)<< endl;

  bool intersects = false;  
  for( const auto& face: this->m_Faces )
  {
      float xmin;
      float ymin;
      float zmin;

      float xmax;
      float ymax;
      float zmax;


      //  xmin = this->m_Points[face.front()];
      // std::cout<< xmin << endl;
      //  ymin = this->m_Points[face[0] +1];
      // std::cout<< ymin << endl;
      //  zmin = this->m_Points[face[0] +2];
      // std::cout<< zmin << endl;

      for( const auto& i: face ){


        int index = ( i * 3 );
        //std::cout<< i << endl;
        // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z)<< endl;

         xmin = std::round(this->m_Points[index]* 10) / 10;
         ymin = std::round(this->m_Points[index + 1]* 10) / 10;
         zmin = std::round(this->m_Points[index + 2]* 10) / 10;

         xmax = std::round(this->m_Points[index]* 10) / 10;
         ymax = std::round(this->m_Points[index + 1]* 10) / 10;
         zmax = std::round(this->m_Points[index + 2]* 10) / 10;

  // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z)<< endl; 
        // std::cout<< std::to_string(x_v) + " " + std::to_string(y_y)+ " " + std::to_string(z_z)<< endl;
  //       std::cout << "" << endl;

        // if(x_v < xmin){
        //   xmin = x_v;
        // }
        // if(x_v > xmax){
        //   xmax = x_v;
        // }

        // if(y_y < ymin){
        //   ymin = y_y;
        // }
        // if(y_y > ymax){
        //   ymax = y_y;
        // }

        // if(z_z < zmin){
        //   zmin = z_z;
        // }
        // if(z_z > zmax){
        //   zmax = z_z;
        // }
        
        //  intersects =  
        //                 x == x_v &&
        //                 // x <= xmax 
        //                 // &&
        //                 // y >= ymin &&
        //                 // y <= ymax 
        //                 // &&
        //                 // z >= zmin &&
        //                 z == z_z
        //                 ;

           
        //  if(intersects){
        // // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
        // std::cout<< "////////////////////" << endl;
        // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z) << endl;
        // // std::cout<< std::to_string(x_x) + " " + std::to_string(y_y)+ " " + std::to_string(z) << endl;
        // std::cout<< "" << endl;
        // std::cout<< "////////////////////" << endl;

        // return intersects;
      // }

        
        
        // std::cout<< std::to_string(this->m_Points[index]) + " " + std::to_string(this->m_Points[index + 1])+ " " + std::to_string(this->m_Points[index + 2])<< endl;
      }


      for( const auto& i: face ){


        int index = ( i * 3 );
        //std::cout<< i << endl;
        // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z)<< endl;

        float x_v = std::round(this->m_Points[index]* 10) / 10;
        float y_y = std::round(this->m_Points[index + 1]* 10) / 10;
        float z_z = std::round(this->m_Points[index + 2]* 10) / 10;

  // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z)<< endl; 
        // std::cout<< std::to_string(x_v) + " " + std::to_string(y_y)+ " " + std::to_string(z_z)<< endl;
  //       std::cout << "" << endl;

        if(x_v <= xmin){
          xmin = x_v;
        }
        if(x_v >= xmax){
          xmax = x_v;
        }

        if(y_y < ymin){
          ymin = y_y;
        }
        if(y_y > ymax){
          ymax = y_y;
        }

        if(z_z <= zmin){
          zmin = z_z;
        }
        if(z_z >= zmax){
          zmax = z_z;
        }
        
         
      }

        
        intersects =  
        xmin > -6.900000 && xmax < 6.900000 && zmin > -6.900000 && zmax < 6.900000  &&
        zmin > -6.500000 && zmax != 6.900000 &&
        x >= xmin && 
                        x <= xmax 
                        // &&
                        // y >= ymin &&
                        // y <= ymax 
                        &&
                        z >= zmin &&
                        z <= zmax
                        ;

           
         if(intersects){
        // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
        std::cout<< "////////////////////" << endl;
        std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z) << endl;
        std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
        // std::cout<< std::to_string(x_x) + " " + std::to_string(y_y)+ " " + std::to_string(z) << endl;
        std::cout<< "" << endl;
        std::cout<< "////////////////////" << endl;

        return intersects;
        // std::cout<< std::to_string(this->m_Points[index]) + " " + std::to_string(this->m_Points[index + 1])+ " " + std::to_string(this->m_Points[index + 2])<< endl;
      }
        // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
        // std::cout<< "" << endl;
        // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;


      // bool intersects =  x >= xmin &&
      //                   x <= xmax &&
      //                   y >= ymin &&
      //                   y <= ymax 
                        // &&
                        // z >= zmin &&
                        // z <= zmax
                        // ;
      // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
      // std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z) << endl;
      // std::cout<< "" << endl;

      

    }
        std::cout<< "" << endl;
        std::cout<< "" << endl;

    // if(intersects){
    //     // std::cout<< std::to_string(xmin) + " " + std::to_string(xmax)+ " " + std::to_string(ymin) + " " + std::to_string(ymax)+ " " + std::to_string(zmin) + " " + std::to_string(zmax) << endl;
    //     std::cout<< "////////////////////" << endl;
    //     std::cout<< std::to_string(x) + " " + std::to_string(y)+ " " + std::to_string(z) << endl;
    //     // std::cout<< std::to_string(x_x) + " " + std::to_string(y_y)+ " " + std::to_string(z) << endl;
    //     std::cout<< "" << endl;
    //     std::cout<< "////////////////////" << endl;
    //   }

    return intersects;

}