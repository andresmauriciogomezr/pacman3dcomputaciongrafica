// =========================================================================
// @author Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
// =========================================================================
#ifndef __PUJ_CGraf__Object__h__
#define __PUJ_CGraf__Object__h__

#include <map>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

namespace PUJ_CGraf
{
  /**
   */
  class Object
  {
  public:
    using TReal = float;
    using TNatural = unsigned short;

    using VReal = std::vector< TReal >;
    using VNatural = std::vector< TNatural >;

    string nombre;

    //TReal transformacion[ 3 ] { 0 };
    TReal traslacion[ 3 ] { 0 };
    TReal rotacion[ 4 ] { 0 };
    TReal color[ 3 ] { 0 };
    bool rotando = false;

    PUJ_CGraf::Object* parent;


  public:
    Object( );
    virtual ~Object( ) = default;

    virtual const TReal* bounding_box( );

    virtual void set_texture(
      const unsigned char* buffer,
      const unsigned int& w, const unsigned int& h
      );

    void add_child( Object* c );
    bool verificarNombre(string nombre);

    Object* child( const unsigned int& i );
    const Object* child( const unsigned int& i ) const;
    unsigned int number_of_children( ) const;

    virtual void draw( ) const;

    virtual void read_from_OBJ( const std::string& in_fname );

    virtual void keypressed( unsigned char k ) {

      if(this->nombre == "t"){

        // std::cout << "///" << endl;
        // std::cout << this->parent->nombre << endl;
        // std::cout << "///" << endl;
        // std::cout << "" << endl;

        if(k == '1'){
          //  this->traslacion[0] = 3;
          //   this->traslacion[1] = 0;
            // bool intersecta = this->parent->vericiar_interseccion_caras(this->traslacion[0], this->traslacion[1], this->traslacion[2]);
            // std::cout << intersecta << endl;
        // ;
            if(!this->parent->vericiar_interseccion_caras(this->traslacion[0], this->traslacion[1], this->traslacion[2]+ .1)){
            this->traslacion[2] = this->traslacion[2] + .1;

            }
        }
        if(k == '2'){
          //  this->traslacion[0] = 3;
          //   this->traslacion[1] = 0;
            if(!this->parent->vericiar_interseccion_caras(this->traslacion[0]+ .1, this->traslacion[1], this->traslacion[2])){
              this->traslacion[0] = this->traslacion[0] + .1;
            }
        }

        if(k == '3'){
          //  this->traslacion[0] = 3;
          //   this->traslacion[1] = 0;
            if(!this->parent->vericiar_interseccion_caras(this->traslacion[0], this->traslacion[1], this->traslacion[2]- .1)){
            this->traslacion[2] = this->traslacion[2] - .1;
            }
        }
        if(k == '4'){
          //  this->traslacion[0] = 3;
          //   this->traslacion[1] = 0;
            if(!this->parent->vericiar_interseccion_caras(this->traslacion[0]- .1, this->traslacion[1], this->traslacion[2])){
            this->traslacion[0] = this->traslacion[0] - .1;
            }
        }

        // std::cout << intersecta << endl;
        // this->parent->vericiar_interseccion_caras(this->traslacion[0], this->traslacion[1], this->traslacion[2]);

        // std::cout << this->traslacion[0]<< endl;
        //  std::cout << this->traslacion[1] << endl;
        //  std::cout << this->traslacion[2] << endl;
        //  std::cout << "" << endl;
      }


      if(this->rotando){
        std::cout << this->nombre << endl;
        if( k == 'x' )
          this->rotate( 10, 0, 1, 0 );
        else if( k == 'X' )
          this->rotate( -10, 0, 1, 0 );
        else if( k == 'y' )
          this->rotate( 10, 0, 0, 1 );
        else if( k == 'Y' )
          this->rotate( -10, 0, 0, 1 );
        else if( k == 'z' )
          this->rotate( 10, 1, 0, 0 );
        else if( k == 'Z' )
          this->rotate( -10, 1, 0, 0 );

      }
      for( auto c: this->m_Children ){
        c->keypressed( k );
      }
    }

    virtual void rotate( float da, float x, float y, float z )
    {
      this->rotacion[ 0 ] += da;
      this->rotacion[ 1 ] = x;
      this->rotacion[ 2 ] = y;
      this->rotacion[ 3 ] = z;
    }

    void _update_bounding_box( );
    virtual bool vericiar_interseccion_caras( float x, float y, float z) const;

  protected:

  protected:
    // Geometry
    TReal m_BBox[ 6 ] { 0 };
    VReal m_Points;
    VReal m_PointNormals;

    std::vector< float > m_Normals;
  std::vector< std::vector< unsigned int > > m_Faces;

    // Topology: size vs indices
    std::map< TNatural, VNatural > m_Cells;

    // Texture
    VReal m_TexCoords;
    std::vector< unsigned char > m_TexBuffer;
    unsigned int m_TexWidth  { 0 };
    unsigned int m_TexHeight { 0 };

    // Children
    std::vector< Object* > m_Children;
  };
} // end namespace

#endif // __PUJ_CGraf__Object__h__

// eof - Object.h
